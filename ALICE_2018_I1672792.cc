// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ZFinder.hh"

namespace Rivet
{

  /// @brief Add a short analysis description here
  class ALICE_2018_I1672792 : public Analysis
  {
  public:

    const double JET_R = 0.5;


    const int npdgid = 999;
    int _photon = 0;
    int _bottom = 0;
    int _charm = 0;
    int _open = 0;
    int _apid = 0;

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(ALICE_2018_I1672792);

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init()
    {
      _photon = getOption<int>("PHOTON_Z", 0);
      _bottom = getOption<int>("BOTTOM", 0);
      _charm = getOption<int>("CHARM", 0);
      _open = getOption<int>("OPEN", 0);
      _apid = getOption<int>("APID", 0);
      // FinalState fs;
      Cut cuts = (Cuts::pT > 0.2 * GeV) && (Cuts::abseta < 0.8);
      // Cut cuts =  (Cuts::pT > 10.*GeV)    && (Cuts::abseta < 2.5);

      FinalState fs1 = FinalState(Cuts::pid == PID::ELECTRON && cuts);
      PromptFinalState pfs1 = PromptFinalState(Cuts::pid == PID::ELECTRON && cuts);
      declare(fs1, "Elecs");
      FinalState fs2 = FinalState(Cuts::pid == -PID::ELECTRON && cuts);
      PromptFinalState pfs2 = PromptFinalState(Cuts::pid == -PID::ELECTRON && cuts);
      declare(fs2, "Posis");

      FastJets fj = FastJets(FinalState(Cuts::abspid != PID::PHOTON && Cuts::abspid != PID::ELECTRON), FastJets::ANTIKT, JET_R);
      declare(fj, "AntiKtJets");


      vector<double> z_bins = {5, 10, 20, 30, 40, 50, 60, 66., 74., 78., 82., 84., 86., 88., 89., 90., 91., 92., 93., 94., 96., 98., 100., 104., 108., 116.};
      // vector<double> z_bins = { 66., 74., 78., 82., 84., 86., 88., 89., 90., 91., 92., 93., 94., 96., 98., 100., 104., 108., 116. };

      // Book histograms
      book(_h["m_ee_pp"], 1, 1, 1);
      book(_h["pT_ee_low_dca_pp"], 2, 1, 1);
      book(_h["pT_ee_low_1_pp"], 4, 1, 1);
      book(_h["pT_ee_low_2_pp"], 5, 1, 1);
      book(_h["pT_ee_high_pp"], 7, 1, 1);
      book(_h["pT_ee_high_dca_pp"], 9, 1, 1);
    }

    bool valid(const Particle &pa)
    {

      if (_open	&& pa.hasParentWith( Cuts::abspid ==441 || Cuts::abspid ==443|| Cuts::abspid ==445|| Cuts::abspid ==553|| Cuts::abspid ==555 || Cuts::abspid ==10441 || Cuts::abspid ==10443 || Cuts::abspid ==10551 || Cuts::abspid ==10553 || Cuts::abspid ==20443 || Cuts::abspid ==20553 || Cuts::abspid ==20555 || Cuts::abspid ==30443 || Cuts::abspid ==100441 || Cuts::abspid ==100443 || Cuts::abspid ==100445 || Cuts::abspid ==100553 || Cuts::abspid ==100555 || Cuts::abspid ==110551 || Cuts::abspid ==120553 || Cuts::abspid ==200553 || Cuts::abspid ==300553 || Cuts::abspid ==9000443 || Cuts::abspid ==9000553 || Cuts::abspid ==9010443 || Cuts::abspid ==9010553 || Cuts::abspid ==9020443)) {
	      return false;
      }


      if (_photon && (
                         (
                             pa.hasParentWith([](const Particle &p)
                                             { return (p.pid() == PID::ZBOSON); })))) // POWHEG virt photon always has pid == 23, 22 might be polluted from decays?
      {
        return true;
      }

      if (_bottom && (
                         (
                             pa.hasParentWith([](const Particle &p)
                                             { return p.genParticle()->status() == 2 && p.isHadron() && p.hasBottom(); }))))
      {
        return true;
      }

      if (_charm && (
                        (
                            pa.hasParentWith([](const Particle &p)
                                            { return p.genParticle()->status() == 2 && p.isHadron() && p.hasCharm(); }))))
      {
        return true;
      }
      if (!_charm && !_bottom && !_photon) {
        return true;
      }
      return false;
    }

    void bin_em(const Particle &l1, const Particle &l2,double weight= 1.0) {
      // Get the true dilepton pair
      const FourMomentum pll = l1.mom() + l2.mom();

      if (pll.pt() < 8 * GeV)
      {
        _h["m_ee_pp"]->fill(pll.mass() / GeV,weight);
      }
      if (pll.mass() < 0.14)
      {
        _h["pT_ee_low_dca_pp"]->fill(pll.pt() / GeV,weight);
      }
      if (0.14 < pll.mass() && pll.mass() < 0.7)
      {
        _h["pT_ee_low_1_pp"]->fill(pll.pt() / GeV,weight);
      }
      if (0.7 < pll.mass() && pll.mass() < 1.1)
      {
        _h["pT_ee_low_2_pp"]->fill(pll.pt() / GeV,weight);
      }
      if (1.1 < pll.mass() && pll.mass() < 2.7)
      {
        _h["pT_ee_high_pp"]->fill(pll.pt() / GeV,weight);
      }
      if (2.7 < pll.mass() && pll.mass() < 3.3)
      {
        _h["pT_ee_high_dca_pp"]->fill(pll.pt() / GeV,weight);
      }
    }
    /**
     * Check if both leptons are valid and not the same
    */
    bool both_valid(const Particle &l1, const Particle &l2) {
      if(!l1.isSame(l2) && valid(l1) && valid(l2)) {
          // APID filter
            if (!_apid || (
                      (l1.hasAncestorWith(Cuts::abspid == _apid)) ||
                      (l2.hasAncestorWith(Cuts::abspid == _apid))))
            {
              return true;
            }
      }
      return false;
    }

    /// Perform the per-event analysis
    void analyze(const Event &event)
    {
      Particles elecs, posis;
      elecs = apply<ParticleFinder>(event, "Elecs").particlesByPt();
      posis = apply<ParticleFinder>(event, "Posis").particlesByPt();


      Jets jets = apply<FastJets>(event, "AntiKtJets").jetsByPt(Cuts::pT > 10*GeV );

        if(jets.size() >= 0){
          _h["xsec"]->fill(0.);
        }
        if(jets.size() >= 1){
          _h["xsec"]->fill(1.);
        }
        if(jets.size() >= 2){
          _h["xsec"]->fill(2.);
        }
        if(jets.size() >= 3){
          _h["xsec"]->fill(3.);
        }
        if(jets.size() >= 4){
          _h["xsec"]->fill(4.);
        }
// We now allow for pure positron events ? only gives a negative contrib...
//      if (elecs.size() < 1)
//        vetoEvent;
//      if (posis.size() < 1)
//        vetoEvent;

      Particle l1;
      Particle l2;

      // Unlike sign contributions
      for (const Particle &l1 : elecs)
      {
        for (const Particle &l2 : posis)
        {
          if( both_valid(l1,l2) ) {
              bin_em(l1,l2);
          }
        }
      }
      // Like sign contributions are subtracted through the negative weight
      for (const Particle &l1 : posis)
      {
        for (const Particle &l2 : posis)
        {
          if( both_valid(l1,l2) ) {
              bin_em(l1,l2,-0.5); // only subbtract half since loop double counts
          }
        }
      }
      for (const Particle &l1 : elecs)
      {
        for (const Particle &l2 : elecs)
        {
          if( both_valid(l1,l2) ) {
              bin_em(l1,l2,-0.5); // only subbtract half since loop double counts
          }
        }
      }



    }

    /// Normalise histograms etc., after the run
    void finalize()
    {
      // normalize(_h);
      scale(_h, crossSectionPerEvent() / (millibarn));

      // scale(_h,crossSection());
      // const double sf = crossSection() / sumOfWeights();
      // scale(_h["mll"], sf);
    }

    //@}

    /// @name Histograms
    //@{
    map<string, Histo1DPtr> _h;
    // map<string, Scatter2DPtr> _s;
    //@}
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ALICE_2018_I1672792);
}

