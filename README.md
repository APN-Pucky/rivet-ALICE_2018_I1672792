# Experimental Rivet Analysis for ALICE 

```
$ make install
```
will build and install `RivetAnalysis.so` with its data to the directory found by `rivet-config`.

You can inspect exemplary results here: <https://apn-pucky.gitlab.io/rivet-ALICE_2018_I1672792/>

## References/Datasets 
- <https://inspirehep.net/literature/1672792> (<https://www.hepdata.net/record/ins1672792>)
