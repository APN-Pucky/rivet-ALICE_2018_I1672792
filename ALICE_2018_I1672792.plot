BEGIN PLOT /ALICE_2018_I1672792/.*
LegendAlign=r
MainPlot=1
RatioPlot=1
END PLOT


BEGIN PLOT /ALICE_2018_I1672792/d01-x01-y01
Title=Inclusive $e^+e^-$ cross section in pp collisions at $\sqrt{s}$ = 7 TeV in the ALICE acceptance as a function of $m_{\rm ee}$.
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_{\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672792/d02-x01-y01
Title=Inclusive $e^+e^-$ cross section in pp collisions at $\sqrt{s}$ = 7 TeV in the ALICE acceptance as a function of $m_{\rm ee}$.
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672792/d04-x01-y01
Title=Inclusive $e^+e^-$ cross section in pp collisions at $\sqrt{s}$ = 7 TeV in the ALICE acceptance as a function of $p_{\rm T,ee}$ for 0.14 < $m_{\rm ee}$ < 0.7 GeV/$c^{2}$.
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672792/d05-x01-y01
Title=Inclusive $e^+e^-$ cross section in pp collisions at $\sqrt{s}$ = 7 TeV in the ALICE acceptance as a function of $p_{\rm T,ee}$ for 0.7 < $m_{\rm ee}$ < 1.1 GeV/$c^{2}$.
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672792/d07-x01-y01
Title=Inclusive $e^+e^-$ cross section in pp collisions at $\sqrt{s}$ = 7 TeV in the ALICE acceptance as a function of $p_{\rm T,ee}$ for 1.1 < $m_{\rm ee}$ < 2.7 GeV/$c^{2}$.
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672792/d09-x01-y01
Title=Inclusive $e^+e^-$ cross section in pp collisions at $\sqrt{s}$ = 7 TeV in the ALICE acceptance as a function of $p_{\rm T,ee}$ for 2.7 < $m_{\rm ee}$ < 3.3 GeV/$c^{2}$.
XLabel=$p_{T,\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_{T,\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT

BEGIN PLOT /ALICE_2018_I1672792/mass_*
Title=Dilepton invariant mass spectrum
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_{\ell\ell}$ [mb / GeV]
LegendAlign=l
LegendXPos=0.50
LegendYPos=1.00
END PLOT